﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FormularzPrzyjecie.Models
{
    [MetadataType(typeof(GoscieMetaData))]
    public partial class Goscie
    {
    }

    public class GoscieMetaData
    {
        [Required(ErrorMessage="Należy podać imię.")]
        public string Imie {get; set;}
        [Required(ErrorMessage="Należy podać nazwisko.")]
        public string Nazwisko {get; set;}
        [Required(ErrorMessage="Proszę zadeklarować swoją obecność.")]
        public bool Obecnosc { get; set; }
        [Required(ErrorMessage="Należy podać, czy przyjdziesz sam czy z kimś.")]
        public bool OsobaTow { get; set; }
    }
}