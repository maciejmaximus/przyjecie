﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FormularzPrzyjecie.Models;

using System.Net.Mail;
using System.Net;

namespace FormularzPrzyjecie.Controllers
{
    public class HomeController : Controller
    {
        private FormularzPrzyjecieDBContext db = new FormularzPrzyjecieDBContext();

        public ViewResult Index()
        {
            int godzina = DateTime.Now.Hour;
            ViewBag.Powitanie = godzina < 18 ? "Dzień dobry" : "Dobry wieczór";
            return View();
        }

        [HttpGet]
        public ViewResult Formularz()
        {
            return View();
        }

        [HttpPost]
        public ViewResult Formularz(Goscie gosc)
        {
            if (ModelState.IsValid)
            {
                //try
                //{
                //    SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                //    client.EnableSsl = true;
                //    client.Timeout = 10000;
                //    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                //    client.UseDefaultCredentials = false;
                //    client.Credentials = new NetworkCredential("formularz.przyjecie@gmail.com", "formularz");
                //    MailMessage msg = new MailMessage();
                //    msg.To.Add("formularz1.przyjecie@gmail.com");
                //    msg.From = new MailAddress("formularz.przyjecie@gmail.com");
                //    msg.Subject = "Powiadomienie nowy formularz";
                //    msg.Body = "Nowa osoba w bazie danych";
                //    client.Send(msg);
                //}
                //catch(Exception ex)
                //{
                //}

                db.Goscie.AddObject(gosc);
                db.SaveChanges();
                return View("Podziekowanie", gosc);
            }

            return View(gosc);
        }

    }
}
